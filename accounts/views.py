from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import LoginForm, SignUpForm

# Create your views here.


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            # create a new user with those values
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            # Login the user
            if user is not None:
                login(request, user)
                return redirect("home")
    # GET Method below
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                # create a new user with those values
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                # login the user
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "Passwords do not match")
    # GET Method below
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)
