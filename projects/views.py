from django.shortcuts import render
from django.shortcuts import redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_details(request, id):
    projects = Project.objects.filter(id=id)
    context = {
        "show_details": projects,
    }
    return render(request, "projects/detail.html", context)


# this is feature 14
@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)  # what does my form = ?
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()  # what does my form = ? is it ProjectModel?
    context = {"form": form}
    return render(request, "projects/create.html", context)
