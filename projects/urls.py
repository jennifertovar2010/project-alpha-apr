from django.urls import path
from projects.views import list_projects, show_details, create_project


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
