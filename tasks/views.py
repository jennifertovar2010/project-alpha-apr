from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.Post)
        if form.is_valid():
            tasks = form.save()
            tasks.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "create.html", context)


@login_required
def show_my_tasks(request):
    user = request.user
    tasks = Task.objects.filter(assignee=user)
    context = {
        "show_my_tasks": tasks,
    }
    return render(request, "tasks/list.html", context)
